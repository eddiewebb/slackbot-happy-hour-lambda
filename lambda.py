import logging
from base64 import b64decode
from random import shuffle
from json import loads
from json import dumps
import time
from multiprocessing.pool import ThreadPool
import boto3
from deps import requests


ENCRYPTED_EXPECTED_TOKEN = "AQECAHgAJhaVLBFUR0PM26w8S3IVuEoiQqkFJmHzNboRGMCVVAAAAHYwdAYJKoZIhvcNAQcGoGcwZQIBADBgBgkqhkiG9w0BBwEwHgYJYIZIAWUDBAEuMBEEDCrDYojsClMkyWeEXQIBEIAzogAp5Vx9Yxy88SBd4F+49IaiTDSdQG+LAPf131hoMd+uB9U2qAav+WgU7CSd0Kh4GOWx" # Enter the base-64 encoded, encrypted Slack command token (CiphertextBlob)
ENCRYPTED_GOOGLE_API_KEY = "AQECAHgAJhaVLBFUR0PM26w8S3IVuEoiQqkFJmHzNboRGMCVVAAAAIYwgYMGCSqGSIb3DQEHBqB2MHQCAQAwbwYJKoZIhvcNAQcBMB4GCWCGSAFlAwQBLjARBAyu/KKp6ZwAmN6NbvYCARCAQkG7ri5nU6KiPNkJCBMIHImrErCzopGGJR50uICR6DhraSJ0YR7z0vXl/+5lSYHWy8jnKTm9x20PumVBcgptIMrfjg=="


kms = boto3.client('kms')
expected_token = kms.decrypt(CiphertextBlob=b64decode(ENCRYPTED_EXPECTED_TOKEN))['Plaintext']
google_api_key = kms.decrypt(CiphertextBlob=b64decode(ENCRYPTED_GOOGLE_API_KEY))['Plaintext']
logger = logging.getLogger()
logger.setLevel(logging.INFO)


forbiddenTypes = ["bakery","cafe","store"];







# lambda_handler defined in aws as function to invoke.
def lambda_handler(event, context):
    response_url = event['response_url']
    try:
      arguments = validate_arguments(event)
      requests.post(arguments['response_url'],data={"text":"I'm looking!"})
      placed_info = search_local_places(arguments)
      thisPlace = pickFirstQualifyingPlace(arguments,placed_info)
      prepareFeaturePhoto(arguments,thisPlace)
      response = buildSlackResponse(arguments,thisPlace)
      postToSlack(arguments['response_url'],response)
      return response
    except Exception as e:
      slackError(response_url,e)




# functions called by entry function


def slackEscape(message):
  rules = {"<":"&lt;",">":"&gt;","&":"&amp",'"':'\\"',"\n":" "}
  cleaned = reduce(lambda x, y: x.replace(y, rules[y]), rules, message)
  return cleaned

def validate_arguments(params):
    logger.debug(params)
    token = params['token']
    if token != expected_token:
        logger.error("Request token (%s) does not match exptected", token)
        raise Exception("Invalid request token")

    arguments = {}
    arguments['user'] = params['user_name']
    arguments['command'] = params['command']
    arguments['channel'] = params['channel_name']
    arguments['response_url'] = params['response_url']
    if 'text' not in params:
        logger.error("no Zip code provided")
        raise Exception("You must provide a zip code")      
    arguments['command_text'] = params['text'].strip()
    if " " in arguments['command_text']:
      command_tokens = arguments['command_text'].split(" ")
      arguments['zip'] = command_tokens.pop(0)
      arguments['keywords'] = command_tokens
    else:
      arguments['zip'] = arguments['command_text']
      arguments['keywords'] = ["beer","drinks","happy"]
    return arguments

def search_local_places(arguments, types = ["bar"]):
  arguments['geo'] = findLatLngForZip(arguments['zip']) 
  placed_info = []
  for type in types:
      rez=getResponseTo(build_search_url(type, arguments))
      if  len( rez['results']) > 0 :
        placed_info=placed_info + rez['results']
        log_time(type + "s merged")
  log_time("all merged")
  return placed_info


def findLatLngForZip(zipcode):
  #turn zipcode into geo code
  geoUrl = "https://maps.googleapis.com/maps/api/geocode/json?key=" + google_api_key + "&address=" + zipcode
  geoInfo = getResponseTo(geoUrl)
  return str(geoInfo['results'][0]['geometry']['location']['lat']) + "," + str(geoInfo['results'][0]['geometry']['location']['lng'])


def build_search_url(type, arguments):
  placesUrl="https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=" + google_api_key;
  #use geoinfo to get nearby places
  placesUrl += "&location=" + arguments['geo']
  placesUrl += "&radius=8000" #5 miles
  placesUrl += "&opennow" #...
  placesUrl += "&type=" + type
  #placesUrl += "&keyword=" + arguments['keyword']
  #print placesUrl
  return placesUrl

def getResponseTo(url):
  logger.debug(url)
  res = requests.get(url)
  if res.status_code == 200:
    logger.debug( res.text)
    resObject = loads(res.text)
    return resObject
  else:
    logger.error("Error: Url  %s responded with %s %s " , url, res.status_code, res.text )
    res.raise_for_status()

def postToSlack(url,payload):
  logger.debug("Sending repsonse below to: " + url)
  logger.debug(payload)
  headers = {"content-type":"application/json"}
  res = requests.post(url,data=dumps(payload),headers=headers)
  if res.status_code == 200:
    logger.info( "success sending to Slack" )
  else:
    logger.error("Error: Url  %s responded with %s %s " , url, res.status_code, res.text )
    res.raise_for_status()



def pickFirstQualifyingPlace(arguments,places):
  # pubs, bars, etc, not chain restaruants
  winner=None;
  logger.info( "Shuffling " + str(len(places)) + " finalists" )
  shuffle(places)
  pool = ThreadPool(processes=20)



  threads = []  
  for place in places:
    thr = pool.apply_async(isPlaceAcceptable, [arguments,place]) # tuple of args for foo
    threads.append(thr)

  for thread in threads:
    #didnt find winner while waiting, or didnt have to wait, check all results.
    winner = thread.get()
    if winner:
      logger.info("Winner found!")
      return winner
  if winner is None :
    raise Exception("No places near match the criteria. Try a another set of keywords or another local zip code == such as  \"/happy-hour 03801 docks\",\"/happy-hour 03820 specials\", etc")



def isPlaceAcceptable(arguments, place):
  logger.info( "checking " + place['name'] )
  for forbiddenType in forbiddenTypes:
    if forbiddenType in place['types']:
      logger.info( "\tforbidden place"   + forbiddenType )
      return False  
  if "rating" not in place or place['rating'] < 3.6:
    logger.info( "\trated too low"  )
    return False;  
  #if "photos" not in place:
  #  logger.info( "\tno photos" )
  #  return False  
  # now most expensive check for matching keywords in reviews, requires another API call
  if is_keyword_mentioned(arguments, place):
    return place
  



def is_keyword_mentioned(arguments,place):
  placeDetails = getResponseTo("https://maps.googleapis.com/maps/api/place/details/json?placeid=" + place['place_id'] + "&key=" + google_api_key);
  if "reviews" not in placeDetails['result']:
    logger.info( "\tno reviews" )
    return False
  for review in placeDetails['result']['reviews']:
    for keyword in arguments['keywords']:
      mention = review['text'].find(keyword)
      if mention > 0 and review['rating'] > 3:
        place['reviews'] = placeDetails['result']['reviews']
        place['url'] = placeDetails['result']['url']
        if "user_ratings_total" in placeDetails['result']:
          place['user_ratings_total'] =  placeDetails['result']['user_ratings_total'] 
        place['reason'] = review['text'];
        if "price_level" not in place:
          place['price_level']=9;
        return True
  logger.info( "\tNo mentions (" + ",".join(arguments['keywords']) + ")" )
  return False

def prepareFeaturePhoto(arguments, place):
  place['photo_url'] = place['icon']
  if place['photos'][0]['photo_reference']:
    place['photo_url'] = "https://maps.googleapis.com/maps/api/place/photo?key=" + google_api_key + "&maxheight=150&photoreference=" + place['photos'][0]['photo_reference']
  


#print slack response
def buildSlackResponse(arguments, thisPlace):
  prices = ["","Free","Cheap","Moderate","Expensive","Very Expensive","6","7","8","Unknown"];
  response = {
    "response_type": "in_channel",
    "attachments": [
        {
            "image_url": str(thisPlace['photo_url']) ,
            "thumb_url": str(thisPlace['photo_url']) , 
            "fallback": "Go to " + str(thisPlace['name']) ,

            "color": "good",

            "title": "Go to " + str(thisPlace['name']) ,
            "title_link": str(thisPlace['url'])  ,

            "text": "You could go to " + str(thisPlace['name'])  +" at " + str(thisPlace['vicinity']) + ". You can also try adding key words for tailored results;",
            "fields":[
                {"title":"Rating",
                 "value": str(thisPlace['rating']) ,
                 "short":True
                },
                {"title":"Price",
                 "value": str(prices[thisPlace['price_level']]) ,
                 "short":True
                },
               
                
                {"title":"Reason",
                 "value": str(slackEscape( thisPlace['reason'])) ,
                 "short":False
                }
              ]
          }
    ]
  }
  if 'website' in thisPlace and thisPlace['website'] is not None:
    website = {
        "title":"Website",
         "value": thisPlace['website'] ,
         "short":False
         }
    response['attachments'][0]['fields'].append(website)
  return response      



def slackError(url,error):
  response = {
    "text":"Whoops! " + str(error)
  }
  postToSlack(url,response)
  raise error
  
def log_time(message):
  logger.debug(message)





if __name__ == '__main__':
  
  """Test that error returned if token does not match expected"""
  event = {"token" : "1234",
    "text" : "03820",
    "team_id" : "T0001",
    "team_domain" : "example",
    "channel_id" : "C2147483705",
    "channel_name" : "test",
    "user_id" : "U2147483697",
    "user_name" : "Steve",
    "command" : "/weather",
    "response_url" : "https://hooks.slack.com/commands/1234/5678"
    }  
  try:
    start = time.time()
    result = lambda_handler(event,None)
    raise Exception("Test failed, accepts invalid tokens")
  except Exception as e:
    end = time.time()
    print("--- %s seconds ---" % (end - start))
    if "token" in str(e):
      print("Pass")  
    else: 
      print("Error")

  """Test that a response is returned with valid token"""  
  event = {
    "token" : "H3sTD2wC9jJuoXlRo5vnlBKP",
    "text" : "03820",
    "team_id" : "T0001",
    "team_domain" : "example",
    "channel_id" : "C2147483705",
    "channel_name" : "test",
    "user_id" : "U2147483697",
    "user_name" : "Steve",
    "command" : "/weather",
    "response_url" : "https://hooks.slack.com/commands/1234/5678"
    } 
  start = time.time()
  result = lambda_handler(event,None)
  end = time.time()
  print("--- %s seconds ---" % (end - start))
  if "You could go to" not in str(result):
    print result
    raise Exception("Test failed, expected valid response")
  else:
    print("Pass")  

  """Test that a missing zip results in error"""  
  event = {
    "token" : "H3sTD2wC9jJuoXlRo5vnlBKP",
    "team_id" : "T0001",
    "team_domain" : "example",
    "channel_id" : "C2147483705",
    "channel_name" : "test",
    "user_id" : "U2147483697",
    "user_name" : "Steve",
    "command" : "/weather",
    "response_url" : "https://hooks.slack.com/commands/1234/5678"
    } 
  start = time.time()
  try:
    result = lambda_handler(event,None)
    raise Exception("Test failed, no zip code was provided")
  except Exception as e:
    end = time.time()
    print("--- %s seconds ---" % (end - start))
    if "zip" in str(e):
      print("Pass")


  """Test that adding keyword results in altered results"""
  event = {
    "token" : "H3sTD2wC9jJuoXlRo5vnlBKP",
    "text" : "03820 sushi",
    "team_id" : "T0001",
    "team_domain" : "example",
    "channel_id" : "C2147483705",
    "channel_name" : "test",
    "user_id" : "U2147483697",
    "user_name" : "Steve",
    "command" : "/weather",
    "response_url" : "https://hooks.slack.com/commands/1234/5678"
    } 
  start = time.time()
  result = lambda_handler(event,None)
  end = time.time()
  print("--- %s seconds ---" % (end - start))
  if "sushi" not in str(result):
    print result
    raise Exception("Test Failed, Adding keyword did not return expected results")
  else:
    print("Pass")  
    
