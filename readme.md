# Happy Hour AWS Lambda for Slack

Returns a suggestion of local establishment with decent ratings and the key feature noted within . (defaults to drinks!)
This was mostly just a pet project for Eddie to explore lambdas, freshen up on python, and eliminate the delay for his team to decide where this weeks happy hour was.

- [Install](#install)
- [Usage](#usage)
- [examples](#examples)
- [Dependnecies into lambda](#dependnecies-into-lambda)


## Install
1. get Google API access keys (required for some api calls) and slack token (ensures only your team can use it)
1. Encrypt ^^ with AWS KMS and drop new ciphers into code
1. Build dependencies into lambda (see (dependencies)[#dependencies-into-lambda])
1. Follow AWS guides to publish the python code as Lambda in AWS behind a service URL (you will need to enc
2. Provide service URL to Slack configuration for your team

## Usage

`/happy-hour [zip-code] [type]`


## examples
`/happy-hour 03820 craft beer`

![Screen Shot 2017-05-19 at 9.55.52 AM.png](https://bitbucket.org/repo/AKra9r/images/1486077094-Screen%20Shot%202017-05-19%20at%209.55.52%20AM.png)


`/happy-hour 03820 pizza`
![Screen Shot 2017-05-19 at 9.55.38 AM.png](https://bitbucket.org/repo/AKra9r/images/3818872647-Screen%20Shot%202017-05-19%20at%209.55.38%20AM.png)

## Dependnecies into lambda
```
    pip install requests -t deps
    zip -r happylambda.zip lambda.py deps/*
  ```